import './style.css'

class App {
    showOrHide(element: Element) {
        element.classList.toggle('displayed')
    }

    run() {
        const checkboxes = document.querySelectorAll('input')
        for (const checkbox of checkboxes) {
            const coffeeName = checkbox.id
            const coffeeImage = document.querySelector('#' + coffeeName + '_image')

            if (coffeeImage !== null) {
                checkbox.onclick = (event) => {
                    this.showOrHide(coffeeImage)
                }
            }
        }
    }
}

window.onload = () => {
    let app = new App()
    app.run()
}
