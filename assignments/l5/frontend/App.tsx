import './style.css';

import React from 'react';

import Choice from './Choice';
import Images from './Images';

export interface ICoffeeInfo {
    id: string;
    name: string;
    img: string;
}

export const coffeeTypes: ICoffeeInfo[] = [
    {
        id: 'americano',
        name: 'Americano',
        img: 'https://cdn.buttercms.com/AB7ud4YSE6nmOX0iGlgA',
    },
    {
        id: 'capuccino',
        name: 'Capuccino',
        img: 'https://thumbs.dreamstime.com/b/capuccino-cup-150676051.jpg',
    },
    {
        id: 'latte_macchiato',
        name: 'Latte Macchiato',
        img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Latte_macchiato.jpg/640px-Latte_macchiato.jpg',
    }
];

export default
function App() {
    return (
        <>
            <h1>Pet project</h1>
            <Choice />
            <Images />
        </>
    );
}
