import React, { MouseEvent, useRef } from "react";
import { useDispatch } from "react-redux";
import { ICoffeeInfo } from "./App";
import { coffeeAction } from "./store";

interface ICheckboxProps {
    coffeeInfo: ICoffeeInfo;
}

export default
function Checkbox({coffeeInfo}: ICheckboxProps) {
    const checkboxElement = useRef<HTMLInputElement>(null);
    const dispatch = useDispatch();

    const onClick = (event: MouseEvent) => {
        const action = checkboxElement.current.checked ? coffeeAction.show : coffeeAction.hide;
        dispatch(action(coffeeInfo.id));
    };

    return (
        <div>
            <input type="checkbox" id={coffeeInfo.id} onClick={onClick} ref={checkboxElement} />
            <label htmlFor={coffeeInfo.id}>{coffeeInfo.name}</label>
        </div>
    );
}
