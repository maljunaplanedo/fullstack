import React from "react";
import { useSelector } from "react-redux";
import { ICoffeeInfo } from "./App";
import { ICoffeeState } from "./store";

import { coffeeTypes } from "./App";

export default
function Images() {
    const selected = useSelector<ICoffeeState, ICoffeeState>(state => state); 

    return (
        <div className="img_container">
            {coffeeTypes.map(info =>
                selected[info.id]
                ? <img src={info.img} key={info.id}></img>
                : null
            )}
        </div>
    );
}
