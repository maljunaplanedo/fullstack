import React from 'react';
import Checkbox from './Checkbox';

import { coffeeTypes } from './App';

export default
function Choice() {
    return (
        <fieldset>
            <legend>Choose your favourite coffee</legend>
            {coffeeTypes.map(info => <Checkbox key={info.id} coffeeInfo={info} />)}
        </fieldset>
    );
}
