import { createSlice, PayloadAction, configureStore } from "@reduxjs/toolkit";

export interface ICoffeeState {
    [id: string]: boolean;
}

const initialState: ICoffeeState = {};

const coffeeReducer = createSlice({
    name: "coffee",
    initialState: initialState,
    reducers: {
        show: (state, action: PayloadAction<string>) => {
            state[action.payload] = true;
        },
        hide: (state, action: PayloadAction<string>) => {
            state[action.payload] = false;
        }
    }
});

export const coffeeAction = coffeeReducer.actions;

export default configureStore(coffeeReducer);
