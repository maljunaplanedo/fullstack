const fs = require('fs');

function getFiles(directory) {
    return new Set(fs.readdirSync(directory))
}

function checkForUpdates(directory, oldFiles) {
    const newFiles = getFiles(directory)

    const setDifference = (a, b) => {
        return new Set([...a].filter(x => !b.has(x)))
    }
    
    const addedFiles = setDifference(newFiles, oldFiles)
    const removedFiles = setDifference(oldFiles, newFiles)

    for (const file of addedFiles) {
        console.log(`Добавлен новый файл ${file}`)
    }
    for (const file of removedFiles) {
        console.log(`Файл ${file} удален`)
    }

    return newFiles
}

function main() {
    if (process.argv.length != 3) {
        console.log("Exactly one argument expected: path");
    }

    const directory = process.argv[2]
    let files = getFiles(directory)

    setInterval(() => {
        files = checkForUpdates(directory, files)
    }, 1000)
}

if (require.main === module) {
    main()
}
