function showOrHide(element) {
    element.classList.toggle('displayed')
}

window.onload = () => {
    const checkboxes = document.querySelectorAll('input')
    for (const checkbox of checkboxes) {
        const coffeeName = checkbox.id
        const coffeeImage = document.querySelector('#' + coffeeName + '_image')

        checkbox.onclick = (event) => {
            showOrHide(coffeeImage)
        }
    }
}
