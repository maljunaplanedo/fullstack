import React, {useEffect, useRef} from "react";

import './style.css';

import {ILaserPosition} from "./LaserState";
import {useDispatch, useSelector} from "react-redux";

import {IRootState, upload, download} from "./store";
import {connect} from "./connection";

export default function App() {
    const pointer = useRef<HTMLDivElement>(null);

    const position = useSelector<IRootState, ILaserPosition>(state => state.download.position);
    const active = useSelector<IRootState, boolean>(state => state.download.active);

    const dispatch = useDispatch();

    useEffect(() => {
        connect(message => {
            dispatch(download(JSON.parse(message.data)));
        });
    }, []);

    const style = {
        left: position.x,
        top: position.y
    };

    const onMouseMove = (event: React.MouseEvent) => {
        dispatch(upload.move({x: event.pageX - 5, y: event.pageY - 5}));
    }

    return (
        <div id="field" onMouseDown={() => dispatch(upload.on())}
             onMouseUp={() => dispatch(upload.off())}
             onMouseMove={onMouseMove}
        >
            <div id="pointer" style={style} ref={pointer}
                 className={active ? "displayed" : undefined}></div>
        </div>
    )
}
