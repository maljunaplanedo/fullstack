import {ILaserState, ILaserPosition} from "./LaserState";
import {Action, combineReducers, configureStore, createSlice, current, PayloadAction, Reducer} from "@reduxjs/toolkit";
import {connection} from "./connection";

const initialState: ILaserState = {position: {x: 0, y: 0}, active: false};

export interface IRootState {
    download: ILaserState;
    upload: ILaserState;
}

const downloadSlice = createSlice({
    name: 'download',
    initialState: initialState,
    reducers: {
        download: (state, action: PayloadAction<ILaserState>) => action.payload
    }
});

const send = (state: ILaserState) => {
    if (connection) {
        connection.send(JSON.stringify(state));
    }
}

const uploadSlice = createSlice({
    name: 'upload',
    initialState: initialState,
    reducers: {
        on: state => {
            state.active = true;
        },
        off: state => {
            state.active = false;
        },
        move: (state, action: PayloadAction<ILaserPosition>) => {
            state.position = action.payload;
        }
    }
})

function sendAfter<A extends Action>(reducer: Reducer<ILaserState, A>) {
    return (state: ILaserState | undefined, action: A) => {
        const result = reducer(state, action);
        if (typeof state == 'undefined' ||
            result.active != state.active ||
            (result.active && result.position != state.position)
        ) {
            send(result);
        }
        return result;
    }
}

uploadSlice.reducer = sendAfter(uploadSlice.reducer);

export const { download } = downloadSlice.actions;
export const upload = uploadSlice.actions;

const downloadReducer = downloadSlice.reducer;
const uploadReducer = uploadSlice.reducer;

const reducer = combineReducers({download: downloadReducer, upload: uploadReducer});

export default configureStore({reducer: reducer});
