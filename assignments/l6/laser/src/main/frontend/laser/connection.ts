export let connection: WebSocket = null;

export const connect = (onMessage: ((this: WebSocket, ev: MessageEvent) => any) | null) => {
    connection = new WebSocket('ws://localhost:8080/communicate');
    connection.onmessage = onMessage;
}
