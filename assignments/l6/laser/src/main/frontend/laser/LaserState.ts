export interface ILaserPosition {
    x: number;
    y: number;
}

export interface ILaserState {
    position: ILaserPosition;
    active: boolean;
}
