package com.maljunaplanedo.laser;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@Component
@Slf4j
public class LaserManager extends TextWebSocketHandler {
    private WebSocketSession master;
    private final Set<WebSocketSession> listeners = new HashSet<>();

    @Override
    public synchronized void afterConnectionEstablished(WebSocketSession session) {
        listeners.add(session);
        if (master == null) {
            master = session;
        }
    }

    @Override
    public synchronized void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        listeners.remove(session);
        if (master == session) {
            var iterator = listeners.iterator();
            if (iterator.hasNext()) {
                master = iterator.next();
            } else {
                master = null;
            }
        }
    }

    @Override
    public synchronized void handleTextMessage(WebSocketSession session, TextMessage message) {
        if (session != master) {
            return;
        }

        listeners.forEach(listener -> {
            try {
                listener.sendMessage(message);
            } catch (IOException exception) {
                log.error(exception.getMessage());
            }
        });
    }
}
