const serve = require('koa-static')
const Koa = require('Koa')
const app = new Koa()

app.use(serve('./public'))

app.listen(3000)
