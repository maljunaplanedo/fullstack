import React from 'react';
import { ICoffeeInfo } from './App';
import Checkbox from './Checkbox';

export type ShowOrHide = (name: string, show: boolean) => void

interface IChoiceProps {
    showOrHide: ShowOrHide;
    coffeeTypes: ICoffeeInfo[];
}

export default
function Choice({showOrHide, coffeeTypes}: IChoiceProps) {
    return (
        <fieldset>
            <legend>Choose your favourite coffee</legend>
            {coffeeTypes.map(info => <Checkbox coffeeInfo={info} showOrHide={showOrHide} />)}
        </fieldset>
    );
}
