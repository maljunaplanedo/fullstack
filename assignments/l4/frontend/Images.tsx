import React from "react";
import { ICoffeeInfo } from "./App";

interface IImagesProps {
    displayed: Set<string>;
    coffeeTypes: ICoffeeInfo[];
}

export default
function Images({displayed, coffeeTypes}: IImagesProps) {
    return (
        <div className="img_container">
            {coffeeTypes.map(info =>
                displayed.has(info.id)
                ? <img src={info.img}></img>
                : null
            )}
        </div>
    );
}
