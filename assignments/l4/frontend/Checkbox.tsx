import React, { MouseEvent, useRef } from "react";
import { ICoffeeInfo } from "./App";

import { ShowOrHide } from "./Choice";

interface ICheckboxProps {
    coffeeInfo: ICoffeeInfo;
    showOrHide: ShowOrHide;
}

export default
function Checkbox({coffeeInfo, showOrHide}: ICheckboxProps) {
    const checkboxElement = useRef<HTMLInputElement>(null);

    const onClick = (event: MouseEvent) => {
        showOrHide(coffeeInfo.id, checkboxElement.current.checked);
    };

    return (
        <div>
            <input type="checkbox" id={coffeeInfo.id} onClick={onClick} ref={checkboxElement} />
            <label htmlFor={coffeeInfo.id}>{coffeeInfo.name}</label>
        </div>
    );
}
